import requests
from lxml.html import fromstring


# This program will output the number of spaces, 
# uppercases and lowercases into console - and write page title into out.txt file
def get_title():
	page = requests.get('https://europa.eu/european-union/about-eu/countries_en')
	contents = fromstring(page.content)
	title = contents.findtext('.//title') # get page title
	spaces = title.count(' ')
	upperCases = 0
	lowerCases = 0
	# calculate upper and lower cases
	for i in title:
	      if(i.islower()):
	            lowerCases=lowerCases+1
	      elif(i.isupper()):
	            upperCases=upperCases+1

	# write to text file

	text_file = open("out.txt", "w")
	text_file.write("Title: {}".format(title))
	text_file.close()

	return {
		'spaces' : spaces,
		'upperCases' : upperCases,
		'lowerCases' : lowerCases,
	}

print(get_title())